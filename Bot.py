# -*- condung: utf-8 -*-

#Librerías de Telegram
# Consultar info  en el siguiente link:
# https://python-telegram-bot.readthedocs.io/en/stable/telegram.html
import telegram
from telegram.ext import *

# token brindado por BotFather, de tec_elementos_bot
telegram_token = telegram.Bot(token="Aquí se escribe el token que genera BotFather")

# Recibe todas las actualizaciones de telegram en base al token
mi_bot_updater = Updater(telegram_token.token)

#Función que escucha los mensajes que se envían al bot
def listener(bot, update):
	id_chat = update.message.chat_id
	mensaje = update.message.text
	print("ID: " + str(id_chat) + " escribio:" + mensaje)

#Función que se ejecua cuando en el bot se pone /start
def start(bot, update, pass_chart_data=True):
	# id del chat de donde proviene el mensaje
	id_chat_recibido = update.message.chat_id
	bot.sendMessage(chat_id=id_chat_recibido, text="Gracias por usarme!")

#handler(manipuladores) del bot
start_handler = CommandHandler('start', start)
listener_handler = MessageHandler(Filters.text, listener)

# Encargado de distribuir las actualizaciones entre los handler
dispatcher = mi_bot_updater.dispatcher

# Agrega los handler el dispather
dispatcher.add_handler(start_handler)
dispatcher.add_handler(listener_handler)

# Inicia las actualizaciones de sondeo de Telegram
mi_bot_updater.start_polling()
# Bloquea el updater hasta que se reccibe algunas señales
mi_bot_updater.idle()

#Inicia el programa de manera infinita
while True:
	pass
